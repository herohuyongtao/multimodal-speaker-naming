for id=13:14

%clear;clc;close all;
clearvars -except id;clc;close all;

addpath data/
addpath results/
addpath utils/

% data params
%id = 1;
file_face_audio_model = sprintf('data/face_audio_model/face_model_%d.mat', id);
file_test = 'data/faces/test.mat';
num_classes = 5;

clearvars -global config;
clearvars -global mem;
init_gpu(1);
global config;
load(file_face_audio_model);
weights = config.weights;
init_j_v5b();
config.weights = weights;

load(file_test);
perm = randperm(size(test_sample, 4));
test_sample = test_sample(:,:,:,perm);
test_tag = test_tag(:,perm);

test_imgs = gpuArray(single(test_sample));
test_labels = gpuArray(single(test_tag));

outputs = zeros(config.output_size, size(test_imgs, 4));
for m = 1:size(test_imgs, 4) / config.batch_size
    print_progress(1, size(test_imgs, 4) / config.batch_size, m, 'testing: ');
    
    val_in = test_imgs(:,:,:,(m-1)*config.batch_size+1:m*config.batch_size);                
    out = compute_output_v5b(val_in);
    outputs(:, (m-1)*config.batch_size+1:m*config.batch_size) = gather(out);
end

% print accuracy
[max_val, estimated_labels] = max(outputs);
[max_val, true_labels] = max(test_labels);
true_labels = double(true_labels);
str = print_accuracy(estimated_labels, true_labels, num_classes);

% save accuracy info to txt
fileID = fopen(sprintf('C:/face_model_acc_%d.txt', id), 'w');
fprintf(fileID, '%s', str);
fclose(fileID);

end