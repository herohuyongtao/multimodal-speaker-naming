%%% generate face samples, based on train/test file list txt
close all;clc;clear;

folder_base = 'data/faces';
num_train = 74636;          % total num of training imgs
num_test = 25453;           % total num of testing imgs
num_each_mat = 5000;        % num of train imgs for each small mat while saving, only for train, test will always be in one mat
num_class = 5;              % num of classes
sz_template = [50 50];      % [height, width], all imgs will resize to this size


%%  load train
sample = zeros(sz_template(1), sz_template(2), 3, num_each_mat);
tag = zeros(num_class, num_each_mat);
idx_track = 0;
idx_file_track = 0;

file = sprintf('%s/train-file-list.txt', folder_base);
[path, label] = textread(file, '%s\t%d', num_train); 

% perm here, important!!!
perm = randperm(length(label));
path = path(perm, :);
label = label(perm, :);

%...
for i=1:num_train
    print_progress(1,num_train,i,'training: ');
    
    img = im2double(imread(path{i}));
    img = imresize(img, sz_template);
    
    sample(:, :, :, idx_track+1) = img;
    tag(label(i)+1, idx_track+1) = 1;
    
    idx_track = idx_track+1;
    
    if idx_track==num_each_mat
        idx_file_track = idx_file_track+1;
        
        save(sprintf('%s/train_%d.mat', folder_base, idx_file_track), 'sample', 'tag', '-v7.3');
        
        sample = zeros(sz_template(1), sz_template(2), 3, num_each_mat);
        tag = zeros(num_class, num_each_mat);
        idx_track = 0;
    elseif i==num_train
        idx_file_track = idx_file_track+1;
        
        sample = sample(:, :, :, 1:idx_track);
        tag = tag(:, 1:idx_track);
        save(sprintf('%s/train_%d.mat', folder_base, idx_file_track), 'sample', 'tag', '-v7.3');
        
        sample = zeros(sz_template(1), sz_template(2), 3, num_each_mat);
        tag = zeros(num_class, num_each_mat);
        idx_track = 0;
    end
end

clearvars sample tag idx_track idx_file_track file path label;


%% load test - all in one mat
test_sample = zeros(sz_template(1), sz_template(2), 3, num_each_mat);
test_tag = zeros(num_class, num_each_mat);

file = sprintf('%s/test-file-list.txt', folder_base);
[path, label] = textread(file, '%s\t%d', num_test);

% perm here, important!!!
perm = randperm(length(label));
path = path(perm, :);
label = label(perm, :);

% ...
for i=1:num_test
    print_progress(1,num_test,i,'testing: ');
    
    img = im2double(imread(path{i}));
    img = imresize(img, sz_template);
    
    test_sample(:, :, :, i) = img;
    test_tag(label(i)+1, i) = 1;
end
save(sprintf('%s/test.mat', folder_base), 'test_sample', 'test_tag', '-v7.3');
