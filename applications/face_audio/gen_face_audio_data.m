%%% generate face-audio samples: only for test
%%% contins 3 mat: face, label, audio (face x 5)

close all;clc;clear;

%% these all params need to setup
face_mat = 'data/faces/test.mat';
audio_mat = 'data/extracted_features/audio-merged-456-3/audio_samples_test_75_20ms.mat';
folder_to_save = 'data/extracted_features';
num_audio_per_face = 5;

%% load data
load(face_mat);
face = test_sample; 
label = test_tag;
clearvars test_sample test_tag;
load(audio_mat);
audio_tmp = sample;
label_tmp = tag;
clearvars sample tag;

%% generate audio, size = face x num_audio_per_face
sz = num_audio_per_face*size(face, 4);
audio = zeros(size(audio_tmp, 1), sz);
for i=1:sz
    print_progress(1, sz, i, 'progress: ');
    
    % label of current face
    idx_in_face = mod(i, size(face, 4));
    if idx_in_face==0
        idx_in_face = size(face, 4);
    end
    tag = label(:, idx_in_face);
    tag = find(tag == max(tag(:))); % 1~6
    
    % random select an audio of same person
    idx = randi(size(audio_tmp, 2));
    tag_audio = label_tmp(:, idx);
    tag_audio = find(tag_audio == max(tag_audio(:))); % 1~6
    while tag_audio~=tag
        idx = randi(size(audio_tmp, 2));
        tag_audio = label_tmp(:, idx);
        tag_audio = find(tag_audio == max(tag_audio(:))); % 1~6
    end
    
    % add to result
    audio(:, i) = audio_tmp(:, idx);
end

%% write to disk
save(sprintf('%s/face_audio_test_456v3.mat', folder_to_save), 'face', 'label', 'audio', '-v7.3');
