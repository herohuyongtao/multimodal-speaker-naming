clc;clear;close all;

addpath applications/face_audio/libsvm/


%% data params
path_train = 'data/face_audio_audio_data/face_audio_audio_train.mat';
path_test = 'data/face_audio_audio_data/face_audio_audio_test.mat';
model_file = 'data/svm_model/model.mat';
flag_online_training = 1;   % 0: load model from disk; 1: online traing and save to disk.
flag_test_training_set = 0; % 0: don't test on the training set; 1: do test...
flag_test_testing_set = 1;  % 0: don't test on the testing set; 2: do test...


%% load training data
fprintf('loading training data...\n');
load(path_train);
% train_samples
% train_labels

% normalize face_audio_feature
trainXC_mean = mean(train_samples');
trainXC_sd = sqrt(var(train_samples')+0.01);
train_samples = (bsxfun(@rdivide, bsxfun(@minus, train_samples', trainXC_mean), trainXC_sd));

training_label_vector = train_labels;
training_instance_matrix = train_samples;


%% train - online
if flag_online_training==1
    fprintf('training...\n');
    model = libsvmtrain(training_label_vector, training_instance_matrix, '-t 2');
    save(model_file, 'model', '-v7.3');
else
    fprintf('loading trained model from disk...\n');
    load(model_file);
end


%% test on training set
if flag_test_training_set==1
    fprintf('testing on the training set...\n');
    [predicted_label_train, accuracy_train, decision_values_train] = ...
        libsvmpredict(training_label_vector, training_instance_matrix, model);
end


%% test on testing set
if flag_test_testing_set==1
    fprintf('testing on the testing set...\n');
    load(path_test);
    % test_samples
    % test_labels
    
    % normalize face_audio_feature
    test_samples = (bsxfun(@rdivide, bsxfun(@minus, test_samples', trainXC_mean), trainXC_sd));  
    
    testing_label_vector = test_labels;
    testing_instance_matrix = test_samples;
    [predicted_label_test, accuracy_test, decision_values_test] = ...
        libsvmpredict(testing_label_vector, testing_instance_matrix, model);
end
