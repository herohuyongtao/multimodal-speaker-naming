%%% get final accuracy by voting (simulate real steaming case)

close all;clc;clear;

addpath data/
addpath results/
addpath utils/
addpath cuda/
addpath applications/face_audio/libsvm/


% data params
file_face_model = 'data/face_model/face_model.mat';
file_face_audio_model = 'data/face_audio_model/face_audio_model.mat';
file_svm_model = 'data/svm_model/model.mat';
file_test = 'data/real_simulate_data/test_simulate.mat';
file_train = 'data/face_audio_audio_data/face_audio_audio_train.mat';
file_log = 'data/log.txt';
num_classes = 5;


clearvars -global config;
clearvars -global mem;
global config mem config2 mem2;
init_gpu(1);
load(file_face_model);
weights = config.weights;
init_j_v5b_for_face_audio_audio();
config2 = config;
config2.weights = weights;
mem2 = mem;

load(file_face_audio_model);
weights = config.weights;
init_j_v5b_for_face_audio_audio();
config.weights = weights;

%% load face_audio_test
load(file_test);

%% load svm model
load(file_svm_model);

%% load train data for svm, just for get its mean and sd
load(file_train);
trainXC_mean = mean(train_samples');
trainXC_sd = sqrt(var(train_samples')+0.01);

num_tmp = 0;
fid = fopen(file_log, 'w');
%% predict for each small range
% face_audio = face_audio(20:end, :); % run part of the data
sz = size(face_audio, 1);
num_correct = 0;
for i=1:sz
    % progress
    print_progress(1, sz, i, 'processing: ');
    
    vote_per_label = zeros(num_classes, 1);
    
    % start voting
    idx_pre = -1;
    for j=1:size(face_audio{i,1}.face_frame_idx, 1)
        if idx_pre==-1 % start a new frame
            if exist('faces_in_current_frame')~=0
                clearvars faces_in_current_frame;
            end
            
            idx_pre = face_audio{i,1}.face_frame_idx(j);
            faces_in_current_frame(:,:,:,1) = face_audio{i,1}.face(:,:,:,j);
        else % previous frame not finish yet
            idx_cur = face_audio{i,1}.face_frame_idx(j);
        
            if idx_cur==idx_pre % still have faces to add for this frame
                faces_in_current_frame(:,:,:,size(faces_in_current_frame,4)+1) = face_audio{i,1}.face(:,:,:,j);
            else % found all faces for this frame
                % generate all samples for all faces
                sample.face = faces_in_current_frame;
                sample.audio = face_audio{i,1}.audio;
                
                % predict for each face-audio pair in sample
                outputs = zeros(num_classes, size(sample.face, 4));
                outputs_face = zeros(num_classes, size(sample.face, 4));
                face_audio_feature = zeros(size(sample.face, 4), 1024 + 75);
                for k=1:size(sample.face, 4)
                    img = gpuArray(single(im2double(sample.face(:,:,:,k))));
%                     imshow(double(img));
                    audio = gpuArray(single(sample.audio));
                    output = compute_output_v5_double_in(img, audio);
                    
                    face_audio_feature(k, :) = [gather(mem.image_audio_feature); sample.audio];
                    
                    output = gather(output);
                    outputs(:, k) = output;
                    
                    outputs_face(:, k) = gather(compute_output_v5b_2_models(img));
                end  
                
                % normalize face_audio_feature
                face_audio_feature = (bsxfun(@rdivide, bsxfun(@minus, face_audio_feature, trainXC_mean), trainXC_sd));
                
                % svm predict here
                label_vector = zeros(size(sample.face, 4), 1); % can be any value, just for stub to call libsvmpredict
                [predicted_label, accuracy_, decision_values] = ...
                    libsvmpredict(label_vector, face_audio_feature, model);
                
                % get which one to vote based on svm
                [max_value, max_idx] = max(decision_values);
                
%                  if max_value>0
                    % get the label of this face at max_idx based on face_audio model
                    tmp = outputs(:, max_idx);
                    [max_value2, max_idx2] = max(tmp);
                    
                    % get the label of this face at max_idx based on face model
                    tmp_face = outputs_face(:, max_idx);
                    [max_value2_face, max_idx2_face] = max(tmp_face); 

                    % update vote_per_label
                    % only vote it if both face and face_audio model has the same label
                    if max_idx2==max_idx2_face && max_value>0
                        vote_per_label(max_idx2) = vote_per_label(max_idx2)+1;
                    else
                        % fprintf(fid, '\t\tskip\n');
                        vote_per_label(max_idx2) = vote_per_label(max_idx2)+0.5;
                    end
%                  end
                
%                 % find the most confident, problem of equality???
%                 lx = find(outputs(:)==max(outputs(:)));
%                 [rx,cx] = ind2sub(size(outputs),lx);
% 
%                 % update vote_per_label
%                 vote_per_label(rx) = vote_per_label(rx)+1;
               

                % next...
                clearvars faces_in_current_frame;
                idx_pre = face_audio{i,1}.face_frame_idx(j);
                faces_in_current_frame(:,:,:,1) = face_audio{i,1}.face(:,:,:,j);
            end
        end 
    end
    
    % vote the last one
    % generate all samples for all faces
    sample.face = faces_in_current_frame;
    sample.audio = face_audio{i,1}.audio;

    % predict for each face-audio pair in sample
    outputs = zeros(num_classes, size(sample.face, 4));
    outputs_face = zeros(num_classes, size(sample.face, 4));
    face_audio_feature = zeros(size(sample.face, 4), 1024 + 75);
    for k=1:size(sample.face, 4)
        img = gpuArray(single(im2double(sample.face(:,:,:,k))));
%       imshow(double(img));
        audio = gpuArray(single(sample.audio));
        output = compute_output_v5_double_in(img, audio);

        face_audio_feature(k, :) = [gather(mem.image_audio_feature); sample.audio];

        output = gather(output);
        outputs(:, k) = output;
        
        outputs_face(:, k) = gather(compute_output_v5b_2_models(img));
    end  

    % normalize face_audio_feature
    face_audio_feature = (bsxfun(@rdivide, bsxfun(@minus, face_audio_feature, trainXC_mean), trainXC_sd));

    % svm predict here
    label_vector = zeros(size(sample.face, 4), 1); % can be any value, just for stub to call libsvmpredict
    [predicted_label, accuracy_, decision_values] = ...
        libsvmpredict(label_vector, face_audio_feature, model);

    % get which one to vote based on svm
    [max_value, max_idx] = max(decision_values);

    % get the label of this face at max_idx based on face_audio model
    tmp = outputs(:, max_idx);
    [max_value2, max_idx2] = max(tmp);

    % get the label of this face at max_idx based on face model
    tmp_face = outputs_face(:, max_idx);
    [max_value2_face, max_idx2_face] = max(tmp_face); 

    % update vote_per_label
    % only vote it if both face and face_audio model has the same label
    if max_idx2==max_idx2_face && max_value>0
        vote_per_label(max_idx2) = vote_per_label(max_idx2)+1;
    else
        % fprintf(fid, '\t\tskip\n');
        vote_per_label(max_idx2) = vote_per_label(max_idx2)+0.5;
    end
    
    % find the most confident label based on voting, problem of equality???
    [max_value3, max_idx3] = max(vote_per_label);
    
    % verify
     if max_value3>0
        if max_idx3-1 == face_audio{i, 1}.label
            num_correct = num_correct+1;
        end
     else
         num_tmp = num_tmp + 1;
     end
     
    fprintf(fid, '%.3f%% (%04d) - %.3f%% - faces-%d \n',...
        i*100/sz, i, num_correct*100/i, size(face_audio{i,1}.face, 4));
end

%% report accuracy
acc = num_correct*100/sz;
fprintf('Accuracy: %.3f\n', acc);
fprintf(fid, 'Accuracy: %.3f\n', acc);

fclose(fid);