clear;clc;close all;

addpath data/
addpath results/
addpath utils/
addpath cuda/

% data params
file_face_audio_model = 'data/face_audio_model/face_audio_model.mat';
file_face = 'data/faces/train_all.mat';
file_audio = 'data/extracted_features/audio-merged-456-3/audio_samples_train_75_20ms.mat';
num_classes = 5;
target_sample_no = 40000;

clearvars -global config;
clearvars -global mem;
global config mem;
init_gpu(1);
load(file_face_audio_model);
weights = config.weights;
init_j_v5b_for_face_audio_audio();
config.weights = weights;

% load training audio data
load(file_audio);

audio_sample_cell = cell(1, num_classes);
[max_val, aud_labels] = max(tag);
for i=1:num_classes
    idx = find(aud_labels == i);
    audio_sample_cell{i} = gpuArray(single((sample(:, idx))));
end

% load all training face data
load(file_face);
% train
% label
[max_val, label] = max(label);

num_of_faces = length(label);
samples = zeros(1024+75, target_sample_no);
labels = zeros(target_sample_no, 1);

% face audio matched samples, label 1
% face audio not matched samples, label 0
for m = 1:target_sample_no / 2
    print_progress(1, target_sample_no / 2, m, 'processing: ');
    
    face_idx = randi(num_of_faces);
    face_input = train(:,:,:,face_idx);
    face_input = gpuArray(single(face_input));
    face_label = label(face_idx);
    
    audio_idx = randi(size(audio_sample_cell{face_label}, 2));
    audio_input_match = audio_sample_cell{face_label}(:, audio_idx);
    
    out = compute_output_v5_double_in(face_input, audio_input_match);
    feature = gather(mem.image_audio_feature);
    samples(:, m) = [feature; gather(audio_input_match)];
    labels(m) = 1;
       
    rand_label = randperm(num_classes);
    unmatch_label_idx = find(rand_label ~= face_label);
    unmatch_label = rand_label(unmatch_label_idx(1));
    audio_idx = randi(size(audio_sample_cell{unmatch_label}, 2));
    audio_input_not_match = audio_sample_cell{unmatch_label}(:, audio_idx);
    out = compute_output_v5_double_in(face_input, audio_input_not_match);
    feature = gather(mem.image_audio_feature);
    samples(:, target_sample_no / 2 + m) = [feature; gather(audio_input_match)];
    labels(target_sample_no / 2 + m) = 0;    
end

perm = randperm(target_sample_no);
train_samples = samples(:,perm);
train_labels = labels(perm);

%% normalize
% no need as compute_output_v5_double_in() already normalized

%% save
save('data/big_bang/face_audio_audio_data/face_audio_audio_train.mat', 'train_samples', 'train_labels', '-v7.3');

%% !!! result for being called in callLibSVM_face_audio_seperately.m