clear;clc;close all;

addpath utils/
addpath data/
addpath cuda/

clearvars -global config;
clearvars -global mem;
init_gpu(1);
init_j_v5b();

% data params
folder_data = 'data/faces';
folder_to_save = 'data/face_model';
str_train_prefix = 'train_';
str_test_prefix = 'test';
idx_range_train_submat = [1 15];
num_classes = 5;
num_passes = 50; % will save a model at end of each pass 

% training
global config;

% load test data
load(sprintf('%s/%s.mat', folder_data, str_test_prefix));
perm = randperm(size(test_sample, 4));
test_sample = test_sample(:,:,:,perm);
test_tag = test_tag(:,perm);

test_imgs = gpuArray(single(test_sample));
test_labels = gpuArray(single(test_tag));

learning_rate = 0.1;
decay = 5e-7 / 10;
points_seen = 0;
fprintf('%s\n', datestr(now, 'dd-mm-yyyy HH:MM:SS FFF'));
for pass = 1:num_passes
    for p = idx_range_train_submat(1):idx_range_train_submat(2)
        load(sprintf('%s/%s%d.mat', folder_data, str_train_prefix, p));
        perm = randperm(size(sample, 4));
        sample = sample(:,:,:,perm);
        tag = tag(:,perm);

        train_imgs = gpuArray(single(sample));
        train_labels = gpuArray(single(tag));
        for i = 1:length(train_labels) / config.batch_size
            eta = learning_rate / (1 + points_seen*decay);
            points_seen = points_seen + config.batch_size;
            in = train_imgs(:,:,:,(i-1)*config.batch_size+1:i*config.batch_size);
            out = train_labels(:,(i-1)*config.batch_size+1:i*config.batch_size);
            check = isnan(in);
            flag = sum(check(:));
            if(flag > 0)
                continue;
            end

            [cost, grad] = core_actions_j_v5b(in, out);
            config.weights.C1 = config.weights.C1 - eta * grad.dC1 / sqrt((config.kernel_size(1, 1) * config.kernel_size(1, 2) * config.conv_hidden_size(1)));
            config.weights.C2 = config.weights.C2 - eta * grad.dC2 / sqrt((config.kernel_size(2, 1) * config.kernel_size(2, 2) * config.conv_hidden_size(2)));
            config.weights.C3 = config.weights.C3 - eta * grad.dC3 / sqrt((config.kernel_size(3, 1) * config.kernel_size(3, 2) * config.conv_hidden_size(3)));
            config.weights.S1 = config.weights.S1 - eta * grad.dS1 / 2;
            config.weights.S2 = config.weights.S2 - eta * grad.dS2 / 2;
            config.weights.W1 = config.weights.W1 - eta * grad.dW1 / sqrt(config.full_hidden_size(1));
            config.weights.W2 = config.weights.W2 - eta * grad.dW2 / sqrt(config.output_size);
            config.weights.bc1 = config.weights.bc1 - eta * grad.dbc1 / sqrt(config.pooling_layer_size(1, 1)*2*config.pooling_layer_size(1, 2)*2);
            config.weights.bc2 = config.weights.bc2 - eta * grad.dbc2 / sqrt(config.pooling_layer_size(2, 1)*2*config.pooling_layer_size(2, 2)*2);
            config.weights.bc3 = config.weights.bc3 - eta * grad.dbc3 / sqrt(config.conv_hidden_size(3));
            config.weights.bs1 = config.weights.bs1 - eta * grad.dbs1 / sqrt(config.pooling_layer_size(1, 1) * config.pooling_layer_size(1, 2));
            config.weights.bs2 = config.weights.bs2 - eta * grad.dbs2 / sqrt(config.pooling_layer_size(2, 1) * config.pooling_layer_size(2, 2));
            config.weights.bf1 = config.weights.bf1 - eta * grad.dbf1 / sqrt(config.full_hidden_size(1));
            config.weights.bf2 = config.weights.bf2 - eta * grad.dbf2 / sqrt(config.output_size);
        end
    end
    
    % save model
    fprintf('saving %s...\n\n', sprintf('face_model_%d.mat', pass));
    save(sprintf('%s/face_model_%d.mat', folder_to_save, pass),'config');

    % save acc report
    outputs = zeros(config.output_size, size(test_imgs, 4));
    for m = 1:size(test_imgs, 4) / config.batch_size
        val_in = test_imgs(:,:,:,(m-1)*config.batch_size+1:m*config.batch_size);                
        out = compute_output_v5b(val_in);
        outputs(:, (m-1)*config.batch_size+1:m*config.batch_size) = gather(out);
    end
    [max_val, estimated_labels] = max(outputs);
    [max_val, true_labels] = max(test_labels);
    str = print_accuracy(estimated_labels,true_labels,num_classes);
    fileID = fopen(sprintf('%s/face_model_acc_%d.txt', folder_to_save, pass), 'w');
    fprintf(fileID, '%s', str);
    fclose(fileID);
end
