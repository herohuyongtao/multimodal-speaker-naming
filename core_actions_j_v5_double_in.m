function [cost, grad] = core_actions_j_v5_double_in(images, audios, labels)
    global config mem;
    
    for m = 1:config.batch_size
        for n = 1:config.chs
            mem.IN1((n-1)*config.kernel_size(1, 1)*config.kernel_size(1, 2)+1:n*config.kernel_size(1, 1)*config.kernel_size(1, 2), (m-1)*(size(mem.IN1, 2)/config.batch_size)+1:m*size(mem.IN1, 2)/config.batch_size) = ...
                    im2col_gpu(images(:,:,n,m), [config.kernel_size(1, 1), config.kernel_size(1, 2)]);
        end
    end
    A1 = sigmoid(bsxfun(@plus, config.weights.C1 * mem.IN1, config.weights.bc1));
	
    % pooling
    P1_z = reshape(accumarray(config.misc.PM1, A1(:), [size(A1,1)*size(A1,2)/4, 1]), size(A1, 2)/4, size(A1, 1));
    P1 = sigmoid(bsxfun(@plus, bsxfun(@times, P1_z, config.weights.S1'), config.weights.bs1'));
    % 2nd conv layer
    % patchify input
    P1_reshape = reshape(P1, config.pooling_layer_size(1, 1), config.pooling_layer_size(1, 2)*config.conv_hidden_size(1)*config.batch_size);
    P1_expand = im2col_gpu(P1_reshape, [config.kernel_size(2, 1), config.kernel_size(2, 2)]);
    P1_expand(:, config.misc.RM1_zidx) = 0;
    mem.IN2 = reshape(accumarray(config.misc.RM1, P1_expand(:), [size(mem.IN2, 1)*size(mem.IN2, 2), 1]), size(mem.IN2, 1), size(mem.IN2, 2));
    
    A2 = sigmoid(bsxfun(@plus, config.weights.C2 * mem.IN2, config.weights.bc2));
    
    % pooling
    P2_z = reshape(accumarray(config.misc.PM2, A2(:), [size(A2,1)*size(A2,2)/4, 1]), size(A2, 2)/4, size(A2, 1));
    P2 = sigmoid(bsxfun(@plus, bsxfun(@times, P2_z, config.weights.S2'), config.weights.bs2'));
    % 3rd conv layer
    P2_reshape = reshape(P2, config.pooling_layer_size(2, 1), config.pooling_layer_size(2, 2)*config.conv_hidden_size(2)*config.batch_size);
    P2_expand = im2col_gpu(P2_reshape, [config.kernel_size(3, 1), config.kernel_size(3, 2)]);
    P2_expand(:, config.misc.RM2_zidx) = 0;
    mem.IN3 = reshape(accumarray(config.misc.RM2, P2_expand(:), [size(mem.IN3, 1)*size(mem.IN3, 2), 1]), size(mem.IN3, 1), size(mem.IN3, 2));    
    
    A3 = sigmoid(bsxfun(@plus, config.weights.C3 * mem.IN3, config.weights.bc3));    
    dm = gpuArray(single(uint8(rand(size(A3)))));
    A3 = A3 .* dm;
    A3 = cat(1, A3, audios);
    
    % fully connected layers
    A4 = sigmoid(bsxfun(@plus, config.weights.W1 * A3, config.weights.bf1));
    dm = gpuArray(single(uint8(rand(size(A4)))));
    A4 = A4 .* dm;
    
    out = cross_entropy(bsxfun(@plus, config.weights.W2 * A4, config.weights.bf2));
    cost = -sum(labels .* log(out));
    %out = sigmoid(bsxfun(@plus, config.weights.W2 * A4, config.weights.bf2));
    %cost = sum((labels - out).^2) / 2;
    cost = sum(cost) / config.batch_size;
    
	% vectorized backprop
    % compute deltas
    dOut = (out - labels) / config.batch_size;
    %dOut = (out - labels) .* derivative_sigm_j(out) / config.batch_size;
    dA4 = (config.weights.W2' * dOut) .* derivative_sigm_j(A4);
    dA3 = (config.weights.W1' * dA4) .* derivative_sigm_j(A3);
    dA3 = dA3(1:config.full_hidden_size(1), :);
    dP2 = (config.weights.C3' * dA3) .* derivative_sigm_j(mem.IN3);

    mem.dP2 = reshape(accumarray(config.misc.accu_idx2, dP2(:)), size(mem.dP2, 1), size(mem.dP2, 2));    
    dA2 = bsxfun(@times, mem.dP2(config.misc.UM2)', config.weights.S2) .* derivative_sigm_j(A2);
    dP1 = (config.weights.C2' * dA2) .* derivative_sigm_j(mem.IN2);
    
    mem.dP1 = reshape(accumarray(config.misc.accu_idx1, dP1(:)), size(mem.dP1, 1), size(mem.dP1, 2));
    dA1 = bsxfun(@times, mem.dP1(config.misc.UM1)', config.weights.S1) .* derivative_sigm_j(A1);
    
    % compute gradients
    grad.dW2 = dOut * A4';
    grad.dW1 = dA4 * A3';
    grad.dC3 = dA3 * mem.IN3';
    grad.dC2 = dA2 * mem.IN2';
    grad.dC1 = dA1 * mem.IN1';
    grad.dS2 = sum(P2_z .* mem.dP2, 1)';
    grad.dS1 = sum(P1_z .* mem.dP1, 1)';
    grad.dbf1 = sum(dA4, 2);
    grad.dbf2 = sum(dOut, 2);
    grad.dbc3 = sum(dA3, 2);
    grad.dbs2 = sum(mem.dP2, 1)';
    grad.dbc2 = sum(dA2, 2);
    grad.dbs1 = sum(mem.dP1, 1)';
    grad.dbc1 = sum(dA1, 2);
end






