function out = compute_output_v5_double_in(images, audios)
    global config mem;
    
    for m = 1:config.batch_size
        for n = 1:config.chs
            mem.IN1((n-1)*config.kernel_size(1, 1)*config.kernel_size(1, 2)+1:n*config.kernel_size(1, 1)*config.kernel_size(1, 2), (m-1)*(size(mem.IN1, 2)/config.batch_size)+1:m*size(mem.IN1, 2)/config.batch_size) = ...
                    im2col_gpu(images(:,:,n,m), [config.kernel_size(1, 1), config.kernel_size(1, 2)]);
        end
    end
    A1 = sigmoid(bsxfun(@plus, config.weights.C1 * mem.IN1, config.weights.bc1));
	
    % pooling
    P1_z = reshape(accumarray(config.misc.PM1, A1(:), [size(A1,1)*size(A1,2)/4, 1]), size(A1, 2)/4, size(A1, 1));
    P1 = sigmoid(bsxfun(@plus, bsxfun(@times, P1_z, config.weights.S1'), config.weights.bs1'));
    % 2nd conv layer
    % patchify input
    P1_reshape = reshape(P1, config.pooling_layer_size(1, 1), config.pooling_layer_size(1, 2)*config.conv_hidden_size(1)*config.batch_size);
    P1_expand = im2col_gpu(P1_reshape, [config.kernel_size(2, 1), config.kernel_size(2, 2)]);
    P1_expand(:, config.misc.RM1_zidx) = 0;
    mem.IN2 = reshape(accumarray(config.misc.RM1, P1_expand(:), [size(mem.IN2, 1)*size(mem.IN2, 2), 1]), size(mem.IN2, 1), size(mem.IN2, 2));
    
    A2 = sigmoid(bsxfun(@plus, config.weights.C2 * mem.IN2, config.weights.bc2));
    
    % pooling
    P2_z = reshape(accumarray(config.misc.PM2, A2(:), [size(A2,1)*size(A2,2)/4, 1]), size(A2, 2)/4, size(A2, 1));
    P2 = sigmoid(bsxfun(@plus, bsxfun(@times, P2_z, config.weights.S2'), config.weights.bs2'));
    % 3rd conv layer
    P2_reshape = reshape(P2, config.pooling_layer_size(2, 1), config.pooling_layer_size(2, 2)*config.conv_hidden_size(2)*config.batch_size);
    P2_expand = im2col_gpu(P2_reshape, [config.kernel_size(3, 1), config.kernel_size(3, 2)]);
    P2_expand(:, config.misc.RM2_zidx) = 0;
    mem.IN3 = reshape(accumarray(config.misc.RM2, P2_expand(:), [size(mem.IN3, 1)*size(mem.IN3, 2), 1]), size(mem.IN3, 1), size(mem.IN3, 2));    
    
    A3 = sigmoid(bsxfun(@plus, config.weights.C3 * mem.IN3, config.weights.bc3));    
    A3 = A3 ./ 2;
    A3 = cat(1, A3, audios);
    
    % fully connected layers
    A4 = sigmoid(bsxfun(@plus, config.weights.W1 * A3, config.weights.bf1));
    A4 = A4 ./ 2;
    
    mem.image_audio_feature = A4;
    
    out = cross_entropy(bsxfun(@plus, config.weights.W2 * A4, config.weights.bf2));
    %out = sigmoid(bsxfun(@plus, config.weights.W2 * A4, config.weights.bf2));
end

