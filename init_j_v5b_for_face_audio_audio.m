function init_j_v5b()
    global config mem;
    config.input_size = [50 50];
    config.output_size = 5;
    config.kernel_size = [15, 15; 5, 5; 7, 7];
    config.conv_hidden_size = [48 256 1024];
    config.full_hidden_size = [1024];
    config.pooling_layer_size = zeros(2, 2);
    config.pooling_layer_size(1, :) = [(config.input_size(1) - config.kernel_size(1, 1) + 1) / 2, (config.input_size(2) - config.kernel_size(1, 2) + 1) / 2];
    config.pooling_layer_size(2, :) = [(config.pooling_layer_size(1, 1) - config.kernel_size(2, 1) + 1) / 2, (config.pooling_layer_size(1, 2) - config.kernel_size(2, 2) + 1) / 2];
    config.batch_size = 1;
    config.chs = 3;

    r = 2; % 1,2,3,4
    %{
    config.weights.C1 = gsingle((rand(config.conv_hidden_size(1), config.kernel_size(1, 1)*config.kernel_size(1, 2)) * 2 - 1) * r);
    config.weights.C2 = gsingle((rand(config.conv_hidden_size(2), config.kernel_size(2, 1)*config.kernel_size(2, 2)*config.conv_hidden_size(1)) * 2 - 1) * r);
    config.weights.C3 = gsingle((rand(config.conv_hidden_size(3), config.kernel_size(3, 1)*config.kernel_size(3, 2)*config.conv_hidden_size(2)) * 2 - 1) * r);
    config.weights.S1 = gsingle((rand(config.conv_hidden_size(1), 1) * 2 - 1) * r);
    config.weights.S2 = gsingle((rand(config.conv_hidden_size(2), 1) * 2 - 1) * r);
    config.weights.W1 = gsingle((rand(config.full_hidden_size(1), config.conv_hidden_size(3)) * 2 - 1) * r);
    config.weights.W2 = gsingle((rand(config.output_size, config.full_hidden_size(1)) * 2 - 1) * r);
    %}
    config.weights.C1 = gpuArray(single(randn(config.conv_hidden_size(1), config.kernel_size(1, 1)*config.kernel_size(1, 2)*config.chs) * r));
    config.weights.C2 = gpuArray(single(randn(config.conv_hidden_size(2), config.kernel_size(2, 1)*config.kernel_size(2, 2)*config.conv_hidden_size(1)) * r));
    config.weights.C3 = gpuArray(single(randn(config.conv_hidden_size(3), config.kernel_size(3, 1)*config.kernel_size(3, 2)*config.conv_hidden_size(2)) * r));
    config.weights.S1 = gpuArray(single(randn(config.conv_hidden_size(1), 1) * r));
    config.weights.S2 = gpuArray(single(randn(config.conv_hidden_size(2), 1) * r));
    config.weights.W1 = gpuArray(single(randn(config.full_hidden_size(1), config.conv_hidden_size(3)) * r));
    config.weights.W2 = gpuArray(single(randn(config.output_size, config.full_hidden_size(1)) * r));
    
%{%}  
    config.weights.C1 = config.weights.C1 / sqrt((config.kernel_size(1, 1) * config.kernel_size(1, 2) * config.conv_hidden_size(1)));
    config.weights.C2 = config.weights.C2 / sqrt((config.kernel_size(2, 1) * config.kernel_size(2, 2) * config.conv_hidden_size(2)));
    config.weights.C3 = config.weights.C3 / sqrt((config.kernel_size(3, 1) * config.kernel_size(3, 2) * config.conv_hidden_size(3)));
    config.weights.S1 = config.weights.S1 / 4;
    config.weights.S2 = config.weights.S2 / 4;
    config.weights.W1 = config.weights.W1 / sqrt(config.full_hidden_size(1));
    config.weights.W2 = config.weights.W2 / sqrt(config.output_size);
  
    config.weights.bc1 = gpuArray(single(zeros(config.conv_hidden_size(1), 1))) + 0.01;  % bias for conv layer 1
    config.weights.bs1 = gpuArray(single(zeros(config.conv_hidden_size(1), 1))) + 0.01;  % bias for pooling layer 1
    config.weights.bc2 = gpuArray(single(zeros(config.conv_hidden_size(2), 1))) + 0.01;
    config.weights.bs2 = gpuArray(single(zeros(config.conv_hidden_size(2), 1))) + 0.01;
    config.weights.bc3 = gpuArray(single(zeros(config.conv_hidden_size(3), 1))) + 0.01;
    config.weights.bf1 = gpuArray(single(zeros(config.full_hidden_size(1), 1))) + 0.01;
    config.weights.bf2 = gpuArray(single(zeros(config.output_size, 1))) + 0.02;

    mem.IN1 = gpuArray(single(zeros(config.kernel_size(1, 1)*config.kernel_size(1, 2)*config.chs, ...
                    (config.input_size(1)-config.kernel_size(1, 1)+1)*(config.input_size(2)-config.kernel_size(1, 2)+1)*config.batch_size)));
    mem.IN2 = gpuArray(single(zeros(config.kernel_size(2, 1)*config.kernel_size(2, 2)*config.conv_hidden_size(1), ...
                    (config.pooling_layer_size(1, 1)-config.kernel_size(2, 1)+1)*(config.pooling_layer_size(1, 2)-config.kernel_size(2, 2)+1)*config.batch_size)));
    mem.IN3 = gpuArray(single(zeros(config.kernel_size(3, 1)*config.kernel_size(3, 2)*config.conv_hidden_size(2), ...
                    (config.pooling_layer_size(2, 1)-config.kernel_size(3, 1)+1)*(config.pooling_layer_size(2, 2)-config.kernel_size(3, 2)+1)*config.batch_size)));
    mem.dP2 = gpuArray(single(zeros(config.pooling_layer_size(2, 1)*config.pooling_layer_size(2, 2)*config.batch_size, config.conv_hidden_size(2))));
    mem.dP1 = gpuArray(single(zeros(config.pooling_layer_size(1, 1)*config.pooling_layer_size(1, 2)*config.batch_size, config.conv_hidden_size(1))));

    tt = reshape(1:config.pooling_layer_size(1, 1)*config.pooling_layer_size(1, 2), ...
                 config.pooling_layer_size(1, 1), config.pooling_layer_size(1, 2));
    ii = im2col(tt, [config.kernel_size(2, 1), config.kernel_size(2, 2)]);
    %ww = ones(size(ii));
    %counts = reshape(accumarray(ii(:), ww(:), [config.pooling_layer_size(1, 1)*config.pooling_layer_size(1, 2) 1], @sum)', ...
    %                 config.pooling_layer_size(1, 1), config.pooling_layer_size(1, 2));
    %config.misc.count1 = gsingle(1./counts);
    
    config.misc.accu_idx1 = [];
    accu_idx1_full = repmat(ii, config.conv_hidden_size(1), config.batch_size);
    for m = 1:config.conv_hidden_size(1)
        for n = 1:config.batch_size
            accu_idx1_full((m-1)*size(ii, 1)+1:m*size(ii, 1), (n-1)*size(ii, 2)+1:n*size(ii, 2)) = ...
                accu_idx1_full((m-1)*size(ii, 1)+1:m*size(ii, 1), (n-1)*size(ii, 2)+1:n*size(ii, 2)) + ((config.batch_size*(m-1)+n-1)*(config.pooling_layer_size(1, 1)*config.pooling_layer_size(1, 2)));
        end
    end
    config.misc.accu_idx1 = gpuArray(single(accu_idx1_full(:)));
    
    
    tt = reshape(1:config.pooling_layer_size(2, 1)*config.pooling_layer_size(2, 2), ...
                 config.pooling_layer_size(2, 1), config.pooling_layer_size(2, 2));
    ii = im2col(tt, [config.kernel_size(3, 1), config.kernel_size(3, 2)]);
    config.misc.accu_idx2 = [];
    accu_idx2_full = repmat(ii, config.conv_hidden_size(2), config.batch_size);
    for m = 1:config.conv_hidden_size(2)
        for n = 1:config.batch_size
            accu_idx2_full((m-1)*size(ii, 1)+1:m*size(ii, 1), (n-1)*size(ii, 2)+1:n*size(ii, 2)) = ...
                accu_idx2_full((m-1)*size(ii, 1)+1:m*size(ii, 1), (n-1)*size(ii, 2)+1:n*size(ii, 2)) + ((config.batch_size*(m-1)+n-1)*(config.pooling_layer_size(2, 1)*config.pooling_layer_size(2, 2)));
        end
    end
    config.misc.accu_idx2 = gpuArray(single(accu_idx2_full(:)));
    
    % pooling matrix    
    sub1 = 1:config.pooling_layer_size(1, 1)*config.pooling_layer_size(1, 2)*config.conv_hidden_size(1)*config.batch_size;
    sub1 = reshape(sub1, config.pooling_layer_size(1, 1), config.pooling_layer_size(1, 2)*config.batch_size, config.conv_hidden_size(1));
    pre_sub1 = zeros(size(sub1, 1)*2, size(sub1, 2)*2, size(sub1, 3));
    for m = 1:size(sub1, 3)
        pre_sub1(:,:,m) = kron(sub1(:,:,m), ones(2));
    end
    config.misc.PM1 = reshape(pre_sub1, size(pre_sub1, 1)*size(pre_sub1, 2), config.conv_hidden_size(1))';
    config.misc.PM1 = gpuArray(single(config.misc.PM1(:)));
    
    sub2 = 1:config.pooling_layer_size(2, 1)*config.pooling_layer_size(2, 2)*config.conv_hidden_size(2)*config.batch_size;
    sub2 = reshape(sub2, config.pooling_layer_size(2, 1), config.pooling_layer_size(2, 2)*config.batch_size, config.conv_hidden_size(2));
    pre_sub2 = zeros(size(sub2, 1)*2, size(sub2, 2)*2, size(sub2, 3));
    for m = 1:size(sub2, 3)
        pre_sub2(:,:,m) = kron(sub2(:,:,m), ones(2));
    end
    config.misc.PM2 = reshape(pre_sub2, size(pre_sub2, 1)*size(pre_sub2, 2), config.conv_hidden_size(2))';
    config.misc.PM2 = gpuArray(single(config.misc.PM2(:)));
    
    % upsampling matrix
    up1 = 1:config.pooling_layer_size(1, 1) * config.pooling_layer_size(1, 2) * config.conv_hidden_size(1)*config.batch_size;
    up1 = reshape(up1, config.pooling_layer_size(1, 1), config.pooling_layer_size(1, 2)*config.batch_size, config.conv_hidden_size(1));
    post_up1 = zeros(size(up1, 1)*2, size(up1, 2)*2, config.conv_hidden_size(1));
    for m = 1:config.conv_hidden_size(1)
        post_up1(:,:,m) = kron(up1(:,:,m), ones(2));
    end
    config.misc.UM1 = reshape(post_up1, size(post_up1, 1)*size(post_up1, 2), config.conv_hidden_size(1));
    config.misc.UM1 = gpuArray(single(config.misc.UM1));
    
    up2 = 1:config.pooling_layer_size(2, 1) * config.pooling_layer_size(2, 2) * config.conv_hidden_size(2)*config.batch_size;
    up2 = reshape(up2, config.pooling_layer_size(2, 1), config.pooling_layer_size(2, 2)*config.batch_size, config.conv_hidden_size(2));
    post_up2 = zeros(size(up2, 1)*2, size(up2, 2)*2, config.conv_hidden_size(2));
    for m = 1:config.conv_hidden_size(2)
        post_up2(:,:,m) = kron(up2(:,:,m), ones(2));
    end
    config.misc.UM2 = reshape(post_up2, size(post_up2, 1)*size(post_up2, 2), config.conv_hidden_size(2));
    config.misc.UM2 = gpuArray(single(config.misc.UM2));
    
    % rearranging matrix
    %1
    valid_kernel_num = (config.pooling_layer_size(1, 1)-config.kernel_size(2, 1)+1)*(config.pooling_layer_size(1, 2)-config.kernel_size(2, 2)+1);
    valid_kernel_num2 = (config.pooling_layer_size(1, 1)-config.kernel_size(2, 1)+1)*(config.pooling_layer_size(1, 2)*2-config.kernel_size(2, 2)+1);
    invalid_kernel_num = valid_kernel_num2 - 2 * valid_kernel_num;
    rm_idx = 1:config.kernel_size(2, 1)*config.kernel_size(2, 2)*config.conv_hidden_size(1)*valid_kernel_num*config.batch_size;
    RM1 = reshape(rm_idx, config.kernel_size(2, 1)*config.kernel_size(2, 2)*config.conv_hidden_size(1), valid_kernel_num*config.batch_size);    
    RM1_final = [];
    for m = 1:config.conv_hidden_size(1)
        for n = 1:config.batch_size            
            section = padarray(RM1((m-1)*config.kernel_size(2, 1)*config.kernel_size(2, 2)+1:m*config.kernel_size(2, 1)*config.kernel_size(2, 2), (n-1)*size(RM1, 2)/config.batch_size+1:n*size(RM1, 2)/config.batch_size), [0, invalid_kernel_num], 'replicate', 'post');
            RM1_final = [RM1_final; section(:)];
        end
    end
    config.misc.RM1 = RM1_final(:);    
    config.misc.RM1 = config.misc.RM1(1:length(config.misc.RM1)-config.kernel_size(2, 1)*config.kernel_size(2, 2)*invalid_kernel_num);
    config.misc.RM1 = gpuArray(single(config.misc.RM1));
    
    config.misc.RM1_zidx = [];
    for m = 1:config.conv_hidden_size(1)*config.batch_size-1
        config.misc.RM1_zidx = [config.misc.RM1_zidx m*valid_kernel_num+(m-1)*invalid_kernel_num+1:m*valid_kernel_num+(m-1)*invalid_kernel_num+invalid_kernel_num];
    end
    config.misc.RM1_zidx = gpuArray(single(config.misc.RM1_zidx));
    
    %2
    valid_kernel_num = (config.pooling_layer_size(2, 1)-config.kernel_size(3, 1)+1)*(config.pooling_layer_size(2, 2)-config.kernel_size(3, 2)+1);
    valid_kernel_num2 = (config.pooling_layer_size(2, 1)-config.kernel_size(3, 1)+1)*(config.pooling_layer_size(2, 2)*2-config.kernel_size(3, 2)+1);
    invalid_kernel_num = valid_kernel_num2 - 2 * valid_kernel_num;
    rm_idx = 1:config.kernel_size(3, 1)*config.kernel_size(3, 2)*config.conv_hidden_size(2)*valid_kernel_num*config.batch_size;
    RM2 = reshape(rm_idx, config.kernel_size(3, 1)*config.kernel_size(3, 2)*config.conv_hidden_size(2), valid_kernel_num*config.batch_size);    
    RM2_final = [];
    for m = 1:config.conv_hidden_size(2)
        for n = 1:config.batch_size            
            section = padarray(RM2((m-1)*config.kernel_size(3, 1)*config.kernel_size(3, 2)+1:m*config.kernel_size(3, 1)*config.kernel_size(3, 2), (n-1)*size(RM2, 2)/config.batch_size+1:n*size(RM2, 2)/config.batch_size), [0, invalid_kernel_num], 'replicate', 'post');
            RM2_final = [RM2_final; section(:)];
        end
    end
    config.misc.RM2 = RM2_final(:);    
    config.misc.RM2 = config.misc.RM2(1:length(config.misc.RM2)-config.kernel_size(3, 1)*config.kernel_size(3, 2)*invalid_kernel_num);
    config.misc.RM2 = gpuArray(single(config.misc.RM2));
    
    config.misc.RM2_zidx = [];
    for m = 1:config.conv_hidden_size(2)*config.batch_size-1
        config.misc.RM2_zidx = [config.misc.RM2_zidx m*valid_kernel_num+(m-1)*invalid_kernel_num+1:m*valid_kernel_num+(m-1)*invalid_kernel_num+invalid_kernel_num];
    end
    config.misc.RM2_zidx = gpuArray(single(config.misc.RM2_zidx));
    
end




