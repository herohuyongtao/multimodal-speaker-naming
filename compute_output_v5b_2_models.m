function out = compute_output_v5b_2_models(images)
    global config2 mem2;
    
    for m = 1:config2.batch_size
        for n = 1:config2.chs
            mem2.IN1((n-1)*config2.kernel_size(1, 1)*config2.kernel_size(1, 2)+1:n*config2.kernel_size(1, 1)*config2.kernel_size(1, 2), (m-1)*(size(mem2.IN1, 2)/config2.batch_size)+1:m*size(mem2.IN1, 2)/config2.batch_size) = ...
                    im2col_gpu(images(:,:,n,m), [config2.kernel_size(1, 1), config2.kernel_size(1, 2)]);
        end
    end
    A1 = sigmoid(bsxfun(@plus, config2.weights.C1 * mem2.IN1, config2.weights.bc1));
	
    % pooling
    P1_z = reshape(accumarray(config2.misc.PM1, A1(:), [size(A1,1)*size(A1,2)/4, 1]), size(A1, 2)/4, size(A1, 1));
    P1 = sigmoid(bsxfun(@plus, bsxfun(@times, P1_z, config2.weights.S1'), config2.weights.bs1'));
    % 2nd conv layer
    % patchify input
    P1_reshape = reshape(P1, config2.pooling_layer_size(1, 1), config2.pooling_layer_size(1, 2)*config2.conv_hidden_size(1)*config2.batch_size);
    P1_expand = im2col_gpu(P1_reshape, [config2.kernel_size(2, 1), config2.kernel_size(2, 2)]);
    P1_expand(:, config2.misc.RM1_zidx) = 0;
    mem2.IN2 = reshape(accumarray(config2.misc.RM1, P1_expand(:), [size(mem2.IN2, 1)*size(mem2.IN2, 2), 1]), size(mem2.IN2, 1), size(mem2.IN2, 2));
    
    A2 = sigmoid(bsxfun(@plus, config2.weights.C2 * mem2.IN2, config2.weights.bc2));
    
    % pooling
    P2_z = reshape(accumarray(config2.misc.PM2, A2(:), [size(A2,1)*size(A2,2)/4, 1]), size(A2, 2)/4, size(A2, 1));
    P2 = sigmoid(bsxfun(@plus, bsxfun(@times, P2_z, config2.weights.S2'), config2.weights.bs2'));
    % 3rd conv layer
    P2_reshape = reshape(P2, config2.pooling_layer_size(2, 1), config2.pooling_layer_size(2, 2)*config2.conv_hidden_size(2)*config2.batch_size);
    P2_expand = im2col_gpu(P2_reshape, [config2.kernel_size(3, 1), config2.kernel_size(3, 2)]);
    P2_expand(:, config2.misc.RM2_zidx) = 0;
    mem2.IN3 = reshape(accumarray(config2.misc.RM2, P2_expand(:), [size(mem2.IN3, 1)*size(mem2.IN3, 2), 1]), size(mem2.IN3, 1), size(mem2.IN3, 2));    
    
    A3 = sigmoid(bsxfun(@plus, config2.weights.C3 * mem2.IN3, config2.weights.bc3));	
    A3 = A3 ./ 2;
    
    % fully connected layers
    A4 = sigmoid(bsxfun(@plus, config2.weights.W1 * A3, config2.weights.bf1));
    A4 = A4 ./ 2;
    
    %mem2.image_feature = A4;
    
    out = cross_entropy(bsxfun(@plus, config2.weights.W2 * A4, config2.weights.bf2));
    %out = sigmoid(bsxfun(@plus, config2.weights.W2 * A4, config2.weights.bf2));
end

