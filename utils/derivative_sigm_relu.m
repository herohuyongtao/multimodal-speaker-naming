function deri = derivative_sigm_relu(a)    
    deri = not(not(max(0, a)));   
end


