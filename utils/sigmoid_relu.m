function sigm = sigmoid_relu(x)    
    sigm = max(0, x);
end
